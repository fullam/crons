#!/bin/sh
set -e
echo "[INFO] Updating data.."
/g/scb2/bork/fullam/bin/Miniconda3/envs/we/bin/python /g/scb2/bork/fullam/bin/crons/update_mag_status_pages.py
echo "[INFO] Syncing to metalog.."
rsync -r /g/scb2/bork/fullam/bin/crons/info_portal_data/tables/* fullam@metalog:/srv/info_portal/tables/
rsync -r /g/scb2/bork/fullam/bin/crons/info_portal_data/static/* fullam@metalog:/srv/info_portal/static/


