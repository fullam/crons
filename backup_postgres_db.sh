cd /g/scb/bork/mocat/database_backups

pg_dump -j 20 -F d metadata -f metadata.dump -U postgres -h koppa -w
mv metadata.dump metadata.dump_`date +\\%Y-\\%m-\\%d`

pg_dump -j 20 -F d progenomes3_db -f progenomes3_db.dump
mv progenomes3_db.dump progenomes3_db.dump_`date +\\%Y-\\%m-\\%d`
find /g/scb/bork/mocat/database_backups -type d -maxdepth 1 -mindepth 1 -type d -mtime +100 -exec echo Delete: {} \;
