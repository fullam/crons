echo 'TAU' `ssh tau "sinfo --format='%12P %15C'" | grep CLUSTER | awk '{ print $2 }' | awk -F'/' '{ print "\t"$1"\t"$2"\t"$4"\t""2000G""\t""1264""\t" }'` `date`
echo 'ETA' `ssh eta "sinfo --format='%12P %15C'" | grep CLUSTER | awk '{ print $2 }' | awk -F'/' '{ print "\t"$1"\t"$2"\t"$4"\t""64G""\t""452""\t" }'` `date`
echo 'UPSILON' `ssh upsilon "sinfo --format='%12P %15C'" | grep CLUSTER | awk '{ print $2 }' | awk -F'/' '{ print "\t"$1"\t"$2"\t"$4"\t""264G""\t""553""\t" }'` `date`
echo 'SIGMA' `ssh sigma "sinfo --format='%12P %15C'" | grep CLUSTER | awk '{ print $2 }' | awk -F'/' '{ print "\t"$1"\t"$2"\t"$4"\t""16G""\t""305""\t" }'` `date`

echo 'MICRO' `ssh micro 'export SGE_QMASTER_PORT=536;export SGE_ROOT=/opt/gridengine; /opt/gridengine/bin/lx-amd64/qhost -q' | grep all | awk '{ print $3 }' | awk -F'/' '{used+=$2;total+=$3} END{print "\t"used"\t"total-used"\t"total"\t""364G""\t""624""\t";}'` `date`
echo 'ALPHA' `ssh alpha 'export SGE_ROOT=/usr/ge62u5; /usr/ge62u5/bin/lx24-amd64/qhost -q' | grep all | awk '{ print $3 }' | awk -F'/' '{used+=$2;total+=$3} END{print "\t"used"\t"total-used"\t"total"\t""1000G""\t""0""\t";}'` `date`
#echo 'BETA' `ssh beta 'export SGE_ROOT=/usr/ge62u5; /usr/ge62u5/bin/lx24-amd64/qhost -q' | grep all | awk '{ print $3 }' | awk -F'/' '{used+=$2;total+=$3} END{print "\t"used"\t"total-used"\t"total"\t""256G""\t""0""\t";}'` `date`
echo 'THETA' `ssh theta 'export SGE_CELL=theta; export SGE_ROOT=/usr/ge62u5; /usr/ge62u5/bin/lx24-amd64/qhost -q' | grep all | awk '{ print $3 }' | awk -F'/' '{used+=$2;total+=$3} END{print "\t"used"\t"total-used"\t"total"\t""1024G""\t""0""\t";}'` `date`
echo 'DELTA' `ssh delta 'export SGE_QMASTER_PORT=6444;export SGE_ROOT=/usr/global/sge; /usr/global/sge/bin/lx24-amd64/qhost -q' | grep all | awk '{ print $3 }' | awk -F'/' '{used+=$2;total+=$3} END{print "\t"used"\t"total-used"\t"total"\t""64G""\t""0""\t";}'` `date`
echo 'EPSILON' `ssh epsilon 'export SGE_QMASTER_PORT=536;export SGE_ROOT=/opt/gridengine; /opt/gridengine/bin/lx-amd64/qhost -q' | grep all | awk '{ print $3 }' | awk -F'/' '{used+=$2;total+=$3} END{print "\t"used"\t"total-used"\t"total"\t""150G""\t""0""\t";}'` `date`

