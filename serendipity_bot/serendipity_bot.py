#!/usr/bin/env python3
import time
import random
import smtplib
import datetime
import pandas as pd
from email.message import EmailMessage


def send_mail(message, recipients, dry_run=False):
    if not recipients:
        return
    for recipient in [recipients]:
        if not recipient:
            continue
        msg = EmailMessage()
        msg.set_content(message)
        msg["Subject"] = "Serendipity Meeting"
        msg["From"] = "serendipity_bot@embl.de"
        msg["To"] = recipient
        s = smtplib.SMTP("smtp.embl.de", 25)
        if not dry_run:
            s.send_message(msg)
        else:
            print(f"\n\nEMAIL:\n{msg}\n\n")
        s.quit()


def load_data(filepath):
    df = pd.read_csv(filepath, sep="\t", comment="#", header=0, index_col=0)
    df = df.dropna(how="all")
    return df.to_dict(orient="index")


def notification_msg(group_names):
    first_names = [x.split(" ")[0] for x in group_names]
    return f"""Dear {', '.join(first_names)},
You have been chosen as a group this week for the serendipity meeting!
The proposed time slot is Thursday 11:00.
Please agree on the venue and, if necessary, adapt the time!
"""


def send_notifications(group_names, serendipity_contacts):
    for group in group_names:
        if len(group) < 3:
            continue
        message = notification_msg(group)
        recipients = [serendipity_contacts[name]["email_address"] for name in group]
        send_mail(message, recipients, dry_run=True)
        time.sleep(1)


def create_groups(people_list, size_of_group=4):
    for i in range(0, len(people_list), size_of_group):
        yield people_list[i : i + size_of_group]


def assign_members_to_groups(members):
    group_len = len(members)
    if group_len % 4 == 0 or group_len < 4:
        return list(create_groups(members))

    for x in [3, 6, 9]:
        if (group_len - x) % 4 == 0:
            members_for_groups_of_3 = x

    groups_of_4 = create_groups(members[0:-members_for_groups_of_3], 4)
    groups_of_3 = create_groups(members[-members_for_groups_of_3:], 3)
    return list(groups_of_4) + list(groups_of_3)


if __name__ == "__main__":
    print(datetime.datetime.now())
    serendipity_contacts = load_data("/g/scb2/bork/fullam/bin/crons/bork_contacts.tsv")
    people2shuffle = list(serendipity_contacts.keys())
    random.shuffle(people2shuffle)
    groups = assign_members_to_groups(people2shuffle)
    print(f"Groups: {groups}")
    send_notifications(groups, serendipity_contacts)
