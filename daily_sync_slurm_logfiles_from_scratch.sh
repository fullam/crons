#!/bin/sh
# Needs to run on embl cluster

# Sync Logfiles from scratch
echo "[INFO] Sync logfiles.."
#rsync -av --exclude 'temp' --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/assemblies  /g/scb2/bork/fullam/mags-logs/
#rsync -av --exclude 'temp' --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/co_assemblies  /g/scb2/bork/fullam/mags-logs/
#rsync -av --exclude 'temp' --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/ps_binning  /g/scb2/bork/fullam/mags-logs/
rsync -av --exclude 'temp' --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/annotation  /g/scb2/bork/fullam/mags-logs/
#rsync -av --exclude 'temp' --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/coa_binning  /g/scb2/bork/fullam/mags-logs/
rsync -avL --exclude 'temp' --exclude 'logs' --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/ps_assembly_binning  /g/scb2/bork/fullam/mags-logs/
rsync -av --exclude 'temp' --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/freeze13_analysis  /g/scb2/bork/fullam/freeze-logs/
find /scratch/fullam/references  -exec touch {} +
#find /scratch/fullam/mags-pipeline/  -exec touch {} +
echo "[INFO] Done!"
