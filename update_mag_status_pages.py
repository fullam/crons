import os
import glob
import json
import subprocess
import pandas as pd
import seaborn as sns
from datetime import date
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

sns.set(style="white")
mags_dir = "/g/scb2/bork/data/MAGs/"
# script_path = os.path.dirname(os.path.realpath(__file__))
out_dir = "/g/scb2/bork/fullam/bin/crons/info_portal_data/"


def get_studies(mags_dir):
    directories = glob.glob(f"{mags_dir}*")
    return [os.path.basename(directory) for directory in directories]


def get_raw_data_count(mags_dir, study):
    return len(get_all_samples(mags_dir, study))


def get_all_samples(mags_dir, study):
    directories = glob.glob(f"{mags_dir}/{study}/raw_data/*")
    return [os.path.basename(directory) for directory in directories]


def get_dirs_in_dir(directory):
    return glob.glob(os.path.join(directory, "*"))


def get_assembly_count(mags_dir, study, assembly_type):
    return len(get_assemblies(mags_dir, study, assembly_type))


def get_assemblies(mags_dir, study, assembly_type):
    assemblies = glob.glob(
        f"{mags_dir}/{study}/{assembly_type}/assemblies/*-assembled.fa.gz"
    )
    return [
        os.path.basename(assembly).replace("-assembled.fa.gz", "")
        for assembly in assemblies
    ]


def create_psa_status_table(studies):
    study_dict = {}
    for study in studies:
        raw_data_count = get_raw_data_count(mags_dir, study)
        # ps_assembly_metaspades = get_assembly_count(mags_dir,
        #                                            study,
        #                                            'psa_metaspades')
        ps_assembly_megahit = get_assembly_count(mags_dir, study, "psa_megahit")
        samples_with_no_assemblies_expected = get_study_blacklist(
            study, get_blacklist("ASSEMBLIES")
        )
        count_samples_with_assemblies_run = ps_assembly_megahit + len(
            samples_with_no_assemblies_expected
        )

        if raw_data_count == 0:
            psa_complete = "N"
        # elif raw_data_count == ps_assembly_metaspades:
        #    psa_complete = 'Y'
        elif raw_data_count == count_samples_with_assemblies_run:
            psa_complete = "Y"
        else:
            psa_complete = "N"

        study_dict[study] = {
            "Raw Data Count": raw_data_count,
            #'Metaspades': ps_assembly_metaspades,
            "Megahit": ps_assembly_megahit,
            "Complete": psa_complete,
        }
    return pd.DataFrame.from_dict(study_dict, orient="index")


def get_rawdata_groups(mags_dir, study, assembly_type):
    path = f"{mags_dir}/{study}/{assembly_type}/group_rawdata/*"
    rawdata_groups = glob.glob(path)
    return [os.path.basename(group) for group in rawdata_groups]


def get_assembly_groups(mags_dir, study, assembly_type):
    path = f"{mags_dir}/{study}/{assembly_type}/assemblies/*"
    assembly_groups = glob.glob(path)
    return [
        os.path.basename(group).split("-assembled.")[0] for group in assembly_groups
    ]


def get_group_rawdata_count(mags_dir, study, assembly_type, group):
    path = f"{mags_dir}/{study}/{assembly_type}/group_rawdata/{group}/*"
    return len(glob.glob(path))


def get_group_assembly_count(mags_dir, study, assembly_type, group):
    path = f"{mags_dir}/{study}/{assembly_type}/assemblies/{group}/*"
    return len(glob.glob(path))


def get_group_coassembly_count(mags_dir, study, assembly_type, group):
    path = f"{mags_dir}/{study}/{assembly_type}/assemblies/{group}-assembled.fa.gz"
    return len(glob.glob(path))


def create_coa_status_table(studies):
    study_list = []
    assembly_type = "coa_megahit"
    for study in studies:
        raw_data_groups = get_rawdata_groups(mags_dir, study, assembly_type)
        assembly_groups = get_assembly_groups(mags_dir, study, assembly_type)
        groups = set(raw_data_groups + assembly_groups)

        for group in groups:
            group_raw_data_count = get_group_rawdata_count(
                mags_dir, study, assembly_type, group
            )
            finished_assembly_count = get_group_coassembly_count(
                mags_dir, study, assembly_type, group
            )

            if finished_assembly_count == 1:
                coa_complete = "Y"
            else:
                coa_complete = "N"

            study_list.append(
                {
                    "Study": study,
                    "Group": group,
                    "Raw Data Count": group_raw_data_count,
                    "Finished Co-Assemblies": finished_assembly_count,
                    "Co-Assembly Complete": coa_complete,
                }
            )

    return pd.DataFrame(study_list)[
        [
            "Study",
            "Group",
            "Raw Data Count",
            "Finished Co-Assemblies",
            "Co-Assembly Complete",
        ]
    ].set_index("Study")


def get_bins(mags_dir, study):
    bin_files = glob.glob(f"{mags_dir}/{study}/psa_*/psb_metabat2/*.fa.gz")
    bin_files += glob.glob(f"{mags_dir}/{study}/psa_*/sag/*.fa.gz")
    return bin_files


def get_samples_from_binlist(binlist):
    try:
        return set(
            [x.split(".grp_single.")[1].split(".psa_megahit")[0] for x in binlist]
        )
    except IndexError:
        return set([x.split(".psa_megahit")[0] for x in binlist])


def create_psa_psb_status_table(studies):
    study_dict = {}
    for study in studies:
        raw_data_count = get_raw_data_count(mags_dir, study)
        bins = get_bins(mags_dir, study)
        bin_count = len(bins)
        samples_with_bins = get_samples_from_binlist(bins)
        samples_with_no_bins_expected = get_study_blacklist(
            study, get_blacklist("BINS")
        )
        count_samples_with_bins = len(samples_with_bins) + len(
            samples_with_no_bins_expected
        )

        if raw_data_count == count_samples_with_bins:
            bin_complete = "Y"
        else:
            bin_complete = "N"

        study_dict[study] = {
            "Raw Data Count": raw_data_count,
            "Bins": bin_count,
            "Samples with bins": count_samples_with_bins,
            "Complete": bin_complete,
        }
    return pd.DataFrame.from_dict(study_dict, orient="index")


def write_df_to_tsv(df, filename, header=True):
    out_file = os.path.join(out_dir, "tables/", filename)
    df.to_csv(out_file, sep="\t", header=header)


def write_dict_to_json(data, filename):
    out_file = os.path.join(out_dir, "tables/", filename)
    with open(out_file, "w") as outfile:
        json.dump(data, outfile)


def add_dict_to_json(new_data, filename):
    out_file = os.path.join(out_dir, "tables/", filename)
    with open(out_file) as json_file:
        data = json.load(json_file)
    now = str(date.today())
    data[now] = new_data
    with open(out_file, "w") as outfile:
        json.dump(data, outfile, indent=4, sort_keys=True)


def make_plot():
    out_file = os.path.join(out_dir, "tables/summary.json")
    with open(out_file) as json_file:
        data = json.load(json_file)
    df = pd.DataFrame.from_dict(data, orient="index").fillna(0).astype(int)
    df = df[
        [
            "total_samples",
            "total_assemblies",
            "total_samples_with_psa_psb_bins",
            "total_samples_with_prodigal",
            "total_samples_with_macrel",
            "total_samples_with_checkm",
            "total_samples_with_checkm2",
            "total_samples_with_gunc",
            "total_samples_with_motus",
            "total_samples_with_taxonomy",
            "total_abricate",
            "total_assembly_mash_sketching",
            "total_assembly_stats",
            "total_bin_mash_sketching",
            "total_deeparg",
            "total_rrna",
            "total_samples_with_rubisco_hmm",
            "total_samples_with_recombinase_hmm",
            "total_samples_with_eggnog_mapper",
        ]
    ]
    df = df.reset_index()

    ax = sns.lineplot(x="index", y="total_assemblies", data=df, label="Assemblies")
    sns.lineplot(x="index", y="total_samples", data=df, ax=ax, label="Samples")
    sns.lineplot(
        x="index",
        y="total_samples_with_psa_psb_bins",
        data=df,
        ax=ax,
        label="Samples with psa bins run",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_prodigal",
        data=df,
        ax=ax,
        label="Samples with prodigal",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_macrel",
        data=df,
        ax=ax,
        label="Samples with macrel",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_checkm",
        data=df,
        ax=ax,
        label="Samples with checkm",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_checkm2",
        data=df,
        ax=ax,
        label="Samples with checkm2",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_gunc",
        data=df,
        ax=ax,
        label="Samples with gunc",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_motus",
        data=df,
        ax=ax,
        label="Samples with motus",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_taxonomy",
        data=df,
        ax=ax,
        label="Samples with taxonomy",
    )
    sns.lineplot(
        x="index", y="total_abricate", data=df, ax=ax, label="Samples with Abricate"
    )
    sns.lineplot(
        x="index",
        y="total_assembly_mash_sketching",
        data=df,
        ax=ax,
        label="Samples with Mash Sketches of Assemblies",
    )
    sns.lineplot(
        x="index", y="total_assembly_stats", data=df, ax=ax, label="Assembly Stats"
    )
    sns.lineplot(
        x="index",
        y="total_bin_mash_sketching",
        data=df,
        ax=ax,
        label="Samples with Mash Sketches of bins",
    )
    sns.lineplot(
        x="index", y="total_deeparg", data=df, ax=ax, label="Samples with DeepARG"
    )
    sns.lineplot(x="index", y="total_rrna", data=df, ax=ax, label="Samples with RRNA")
    sns.lineplot(
        x="index",
        y="total_samples_with_rubisco_hmm",
        data=df,
        ax=ax,
        label="Samples with Rubisco_HMM",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_recombinase_hmm",
        data=df,
        ax=ax,
        label="Samples with Recombinase_HMM",
    )
    sns.lineplot(
        x="index",
        y="total_samples_with_eggnog_mapper",
        data=df,
        ax=ax,
        label="Samples with Eggnog_Mapper",
    )
    sns.set_context("notebook", rc={"lines.linewidth": 2.5, "xtick.bottom": False})
    sns.despine()
    sns.set_palette("tab20b")
    fig = ax.get_figure()
    ax.tick_params(axis="x", rotation=90)
    ax.xaxis.label.set_visible(False)
    ax.xaxis.set_major_locator(ticker.AutoLocator())
    ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
    ax.yaxis.label.set_visible(False)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(loc="center left", bbox_to_anchor=(1, 0.5))
    out_file = os.path.join(out_dir, "static/summary.png")
    fig.set_figheight(5)
    fig.set_figwidth(12)
    fig.savefig(out_file, dpi=150, bbox_inches="tight")


def write_list_to_file(list_to_write, filename):
    out_file = os.path.join(out_dir, "tables/", filename)
    with open(out_file, "w") as f:
        f.write("\n".join(list_to_write))


def get_blacklist(list_type):
    if list_type == "BINS":
        blacklist = os.path.join(out_dir, "bin_blacklist.tsv")
    elif list_type == "ASSEMBLIES":
        blacklist = os.path.join(out_dir, "assembly_blacklist.tsv")
    return pd.read_csv(blacklist, header=0, sep="\t")


def get_study_blacklist(study, df):
    return df[df["STUDY"] == study]["SAMPLE"].tolist()


def update_file_lists(study):
    files_dict = {}
    status_dict = {}
    samples = get_all_samples(mags_dir, study)
    study_bin_blacklist = get_study_blacklist(study, sample_bin_blacklist)
    study_assembly_blacklist = get_study_blacklist(study, sample_assembly_blacklist)
    for sample in samples:
        study_dir = f"{mags_dir}/{study}"
        annotation_dir = f"{mags_dir}/annotations/{study}/psa_*/"
        raw_data_files = glob.glob(f"{study_dir}/raw_data/{sample}/*")
        assembly_files = glob.glob(
            f"{study_dir}/psa_*/assemblies/{sample}-assembled.fa.gz"
        )
        prodigal_files = glob.glob(
            f"{annotation_dir}/prodigal/{sample}.*.prodigal.gff.gz"
        )
        eggnog_files = glob.glob(
            f"{annotation_dir}/eggnog-mapper/{sample}/{sample}.emapper.hits.gz"
        )
        motus_files = glob.glob(
            f'/g/bork5/mocat/ngless-processing/motus-v2.5/*{study.replace("internal_", "")}*/outputs/*'
        )
        macrel_files = glob.glob(f"{annotation_dir}/macrel/{sample}.smorfs.faa")
        rubisco_hmm_files = glob.glob(
            f"{annotation_dir}/hmmer/rubisco_hmm/{sample}.rubisco_hmm.tsv"
        )
        recombinase_hmm_files = glob.glob(
            f"{annotation_dir}/hmmer/recombinase_hmm/{sample}.recombinase_hmm.tsv"
        )
        expect_bins = "Yes"
        if sample in study_assembly_blacklist:
            assembly_status = "NA"
        elif assembly_files:
            assembly_status = "Yes"
        else:
            assembly_status = "No"

        if sample in study_bin_blacklist:
            bin_status = "NA"
            checkm_files = ""
            checkm2_files = ""
            bin_files = ""
            taxonomy_files = ""
            gunc_files = ""
        else:
            checkm_files = glob.glob(
                f"{annotation_dir}/checkm/grp_single/{sample}.checkm.tsv"
            )
            checkm2_files = glob.glob(
                f"{annotation_dir}/checkm2/grp_single/{sample}.checkm2.tsv"
            )
            gunc_files = glob.glob(
                f"{annotation_dir}/gunc/grp_single/{sample}.GUNC.maxCSS_level.tsv"
            )
            taxonomy_files = glob.glob(
                f"{annotation_dir}/taxonomy/grp_single/{sample}.taxonomy_annotation.tsv"
            )
            bin_files = glob.glob(f"{study_dir}/psa_*/psb_metabat2/{sample}.*.fa.gz")
            bin_files += glob.glob(f"{study_dir}/psa_*/sag/{sample}.*.fa.gz")
            if bin_files:
                bin_status = "Yes"
            else:
                bin_status = "No"

        files_dict[sample] = {
            "raw_data": raw_data_files,
            "assemblies": assembly_files,
            "bins": bin_files,
            "prodigal": prodigal_files,
            "checkm": checkm_files,
            "checkm2": checkm2_files,
            "gunc": gunc_files,
            "motus": motus_files,
            "macrel": macrel_files,
            "taxonomy": taxonomy_files,
            "rubisco_hmm": rubisco_hmm_files,
            "recombinase_hmm": recombinase_hmm_files,
            "eggnog_mapper": eggnog_files,
        }

        status_dict[sample] = {
            "raw_data": "Yes" if raw_data_files else "No",
            "assemblies": assembly_status,
            "bins": bin_status,
            "prodigal": "Yes" if prodigal_files else "No",
            "checkm": "Yes" if checkm_files else "No",
            "checkm2": "Yes" if checkm2_files else "No",
            "gunc": "Yes" if gunc_files else "No",
            "motus": "Yes" if motus_files else "No",
            "macrel": "Yes" if macrel_files else "No",
            "taxonomy": "Yes" if taxonomy_files else "No",
            "rubisco_hmm": "Yes" if rubisco_hmm_files else "No",
            "recombinase_hmm": "Yes" if recombinase_hmm_files else "No",
            "eggnog_mapper": "Yes" if eggnog_files else "No",
        }
    write_dict_to_json(files_dict, f"file_lists/{study}.json")
    write_dict_to_json(status_dict, f"status_lists/{study}.json")


def run_shell_cmd(cmd):
    return subprocess.check_output(cmd, shell=True, universal_newlines=True)


def create_annotation_status_table(studies):
    study_dict = {}
    for study in studies:
        annotation_dir = f"{mags_dir}/annotations/{study}/psa_*/"
        raw_data_count = get_raw_data_count(mags_dir, study)

        prodigal_count = len(glob.glob(f"{annotation_dir}/prodigal/*.prodigal.gff.gz"))
        macrel_count = len(glob.glob(f"{annotation_dir}/macrel/*.smorfs.faa"))
        eggnog_count = len(
            glob.glob(f"{annotation_dir}/eggnog-mapper/*/*.emapper.hits.gz")
        )

        checkm_count = len(glob.glob(f"{annotation_dir}/checkm/grp_single/*.tsv"))
        checkm2_count = len(glob.glob(f"{annotation_dir}/checkm2/grp_single/*.tsv"))
        gunc_count = len(
            glob.glob(f"{annotation_dir}/gunc/grp_single/*.maxCSS_level.tsv")
        )
        # checkm_count = len(set([x.split('.grp_single.')[1].split('.psa_megahit')[0] for x in checkm_files]))
        taxonomy_count = len(glob.glob(f"{annotation_dir}/taxonomy/grp_single/*.tsv"))

        motus_files = len(
            glob.glob(
                f'/g/bork5/mocat/ngless-processing/motus-v2.5/*{study.replace("internal_", "")}*/outputs/*'
            )
        )

        abricate_count = len(glob.glob(f"{annotation_dir}/abricate/*.vfdb.tsv"))
        assembly_mash_sketching_count = len(
            glob.glob(f"{annotation_dir}/assembly_mash_sketching/*.msh")
        )
        assembly_stats_count = len(glob.glob(f"{annotation_dir}/assembly_stats/*.tsv"))
        bin_mash_sketching_count = len(
            glob.glob(f"{annotation_dir}/bin_mash_sketching/*")
        )
        deeparg_count = len(glob.glob(f"{annotation_dir}/deeparg/*.mapping.ARG"))
        rrna_count = len(glob.glob(f"{annotation_dir}/rrna/*"))
        rubisco_hmm_count = len(glob.glob(f"{annotation_dir}/hmmer/rubisco_hmm/*.tsv"))
        recombinase_hmm_count = len(
            glob.glob(f"{annotation_dir}/hmmer/recombinase_hmm/*.tsv")
        )

        if motus_files:
            motus_count = raw_data_count
        else:
            motus_count = 0

        study_dict[study] = {
            "Raw Data Count": raw_data_count,
            "Prodigal": prodigal_count,
            "CheckM": checkm_count,
            "CheckM2": checkm2_count,
            "GUNC": gunc_count,
            "Macrel": macrel_count,
            "Motus": motus_count,
            "Taxonomy": taxonomy_count,
            "Abricate": abricate_count,
            "Assembly Mash": assembly_mash_sketching_count,
            "Assembly Stats": assembly_stats_count,
            "Bin Mash": bin_mash_sketching_count,
            "DeepARG": deeparg_count,
            "RRNA": rrna_count,
            "Rubisco_HMM": rubisco_hmm_count,
            "Recombinase_HMM": recombinase_hmm_count,
            "Eggnog_Mapper": eggnog_count,
        }
    return pd.DataFrame.from_dict(study_dict, orient="index")


def create_summary_annotation_status_table(annotation_status):
    df = annotation_status.copy(deep=True)
    df["Prodigal"] = df.apply(
        lambda x: "Y" if x["Prodigal"] == x["Raw Data Count"] else "N", axis=1
    )
    df["CheckM"] = df.apply(
        lambda x: "Y" if x["CheckM"] == x["Raw Data Count"] else "N", axis=1
    )
    df["CheckM2"] = df.apply(
        lambda x: "Y" if x["CheckM2"] == x["Raw Data Count"] else "N", axis=1
    )
    df["Macrel"] = df.apply(
        lambda x: "Y" if x["Macrel"] == x["Raw Data Count"] else "N", axis=1
    )
    df["Motus"] = df.apply(
        lambda x: "Y" if x["Motus"] == x["Raw Data Count"] else "N", axis=1
    )
    df["Taxonomy"] = df.apply(
        lambda x: "Y" if x["Taxonomy"] == x["Raw Data Count"] else "N", axis=1
    )
    df["Rubisco_HMM"] = df.apply(
        lambda x: "Y" if x["Rubisco_HMM"] == x["Raw Data Count"] else "N", axis=1
    )
    df["Recombinase_HMM"] = df.apply(
        lambda x: "Y" if x["Recombinase_HMM"] == x["Raw Data Count"] else "N", axis=1
    )
    df["Eggnog_Mapper"] = df.apply(
        lambda x: "Y" if x["Eggnog_Mapper"] == x["Raw Data Count"] else "N", axis=1
    )
    return df


def get_cpu_hours():
    cpu_hours_file = os.path.join(out_dir, "cpu_hours_total.txt")
    with open(cpu_hours_file, "r") as f:
        return int(f.readlines()[0].strip().split(".")[0])


def make_donut_plot(total, value, out_file):
    plt.clf()
    plt.pie([value, total - value], colors=["#4055B1", "#b1b5c7"])
    my_circle = plt.Circle((0, 0), 0.7, color="white")
    p = plt.gcf()
    p.gca().add_artist(my_circle)
    plt.text(
        0,
        0,
        f"{int((value * 100) / total )}%",
        horizontalalignment="center",
        verticalalignment="center",
        fontsize=20,
    )
    p.savefig(out_file, dpi=150, bbox_inches="tight")


def make_status_plot(data):
    plt.clf()
    total_samples_with_bins = int(data["total_samples"]) - int(
        data["total_samples_with_no_bins_expected"]
    )
    nice_labels = {
        "total_abricate": "Abricate",
        "total_assemblies": "Assemblies",
        "total_assembly_mash_sketching": "Mash Assemblies",
        "total_assembly_stats": "Assembly Stats",
        "total_bin_mash_sketching": "Mash Bins",
        "total_deeparg": "DeepARG",
        "total_rrna": "RRNA",
        "total_samples": "TOTAL SAMPLES",
        "total_samples_with_checkm": "CheckM",
        "total_samples_with_checkm2": "CheckM2",
        "total_samples_with_gunc": "GUNC",
        "total_samples_with_macrel": "Macrel",
        "total_samples_with_motus": "Motus",
        "total_samples_with_prodigal": "Prodigal",
        "total_samples_with_psa_psb_bins": "Bins",
        "total_samples_with_taxonomy": "Taxonomy",
        "total_samples_with_rubisco_hmm": "Rubisco_HMM",
        "total_samples_with_recombinase_hmm": "Recombinase_HMM",
        "total_samples_with_eggnog_mapper": "Eggnog_Mapper",
    }
    data = {nice_labels[y]: data[y] for y in nice_labels}
    df = pd.DataFrame.from_dict(data, orient="index", columns=["count"]).astype(int)
    df = df.sort_values(by="count").reset_index()
    ax2 = sns.barplot(
        x="count",
        y="index",
        data=df,
        label="Assemblies",
        palette=sns.light_palette("#4055B1", len(df)),
    )
    # plt.axvline(total_samples_with_bins, color="#db654d")
    ax2.xaxis.label.set_visible(False)
    ax2.yaxis.label.set_visible(False)
    sns.despine()
    fig2 = ax2.get_figure()
    fig2.set_figheight(5)
    fig2.set_figwidth(12)
    out_file = os.path.join(out_dir, "static/status.png")
    fig2.savefig(out_file, dpi=150, bbox_inches="tight")


def make_status_plot2(data):
    plt.clf()
    total_samples_with_bins = int(data["total_samples"]) - int(
        data["total_samples_with_no_bins_expected"]
    )
    nice_labels = {
        "total_assemblies": "Assemblies",
        "total_samples": "TOTAL SAMPLES",
        "total_samples_with_checkm": "QC Done",
        "total_samples_with_motus": "Motus Taxonomy",
        "total_samples_with_prodigal": "Gene Calling",
        "total_samples_with_psa_psb_bins": "Bins",
        "total_samples_with_taxonomy": "Taxonomy",
    }
    data = {nice_labels[y]: data[y] for y in nice_labels}
    df = pd.DataFrame.from_dict(data, orient="index", columns=["count"]).astype(int)
    df = df.sort_values(by="count").reset_index()
    ax2 = sns.barplot(
        x="count",
        y="index",
        data=df,
        label="Assemblies",
        palette=sns.light_palette("#4055B1", len(df)),
    )
    # plt.axvline(total_samples_with_bins, color="#db654d")
    ax2.xaxis.label.set_visible(False)
    ax2.yaxis.label.set_visible(False)
    sns.despine()
    fig2 = ax2.get_figure()
    fig2.set_figheight(5)
    fig2.set_figwidth(12)
    out_file = os.path.join(out_dir, "static/status2.png")
    fig2.savefig(out_file, dpi=150, bbox_inches="tight")


if __name__ == "__main__":
    # Some of these folders are left as symlinks for luis' global data because they were renamed
    exclude_folders = [
        "annotations",
        "tmp_assembly_dir",
        "metadata",
        "tmp_assembly_dir_human",
        "tmp_assemblies",
        "quarantine",
        "Bahram_2018_topsoil",
        "Ellegaard_2019_honeyBee",
        "Espinoza_2018",
        "FMT_FATLOOSE",
        "FMT_Kumar2017",
        "FMT_Smillie",
        "Franzosa_2018",
        "Fukuyama_2017",
        "Gibson2016",
        "Hendriksen_2019_sewage",
        "HoonHanks_2018_dog",
        "LeBastard_2019_ESBL-E",
        "MetaHit",
        "MMPC",
        "Rosa_2018_Indonesian_Liberian",
        "Sankaranarayanan_2015_nativeAmericans",
        "Wang_2019_weanlingPig",
        "studies-20_PRJEB24179",
        "studies-20_PRJEB6422",
        "JGI_soil_amended",
        "Zhang_2018_panda",
        "PRJNA356291_animal_cow",
        "PRJNA543206_chicken",
    ]
    studies = [x for x in get_studies(mags_dir) if x not in exclude_folders]
    studies = [x for x in studies if "internal_" not in x]
    cmd = f"""find {mags_dir}/*/records -type f -mtime -1 -exec dirname {{}} \; | sort --unique"""
    modified_studies = run_shell_cmd(cmd).split("\n")
    modified_studies = [x.split("/")[-2] for x in modified_studies if x]
    # modified_studies = studies
    # modified_studies = ['Xiao_2015_mouse']

    sample_bin_blacklist = get_blacklist("BINS")
    sample_assembly_blacklist = get_blacklist("ASSEMBLIES")

    print("[INFO] Updating sample status.")
    for i, study in enumerate(modified_studies):
        print(f"Updating study {i + 1} of {len(modified_studies)}: {study}.")
        update_file_lists(study)

    print("[INFO] Updating sample annotation status.")
    annotation_status = create_annotation_status_table(studies)
    write_df_to_tsv(annotation_status, "annotation_status.tsv")
    summary_annotation_status = create_summary_annotation_status_table(
        annotation_status
    )
    write_df_to_tsv(summary_annotation_status, "summary_annotation_status.tsv")

    print("[INFO] Updating psa status.")
    psa_status = create_psa_status_table(studies)
    write_df_to_tsv(psa_status, "psa_status.tsv")

    print("[INFO] Updating coa status.")
    coa_status = create_coa_status_table(studies)
    write_df_to_tsv(coa_status, "coa_status.tsv")

    print("[INFO] Updating psa_psb status.")
    psa_psb_status = create_psa_psb_status_table(studies)
    write_df_to_tsv(psa_psb_status, "psa_psb_status.tsv")

    print("[INFO] Updating summary status.")
    total_studies = len(studies)
    total_raw_data = psa_status["Raw Data Count"].sum()
    total_assemblies = psa_status["Megahit"].sum()
    total_coassemblies = coa_status["Finished Co-Assemblies"].sum()
    total_psa_psb_bins = psa_psb_status["Bins"].sum()
    total_samples_with_psa_psb_bins = psa_psb_status["Samples with bins"].sum()
    total_prodigal = annotation_status["Prodigal"].sum()
    total_macrel = annotation_status["Macrel"].sum()
    total_checkm = annotation_status["CheckM"].sum()
    total_checkm2 = annotation_status["CheckM2"].sum()
    total_gunc = annotation_status["GUNC"].sum()
    total_motus = annotation_status["Motus"].sum()
    total_taxonomy = annotation_status["Taxonomy"].sum()
    total_abricate = annotation_status["Abricate"].sum()
    total_assembly_mash_sketching = annotation_status["Assembly Mash"].sum()
    total_assembly_stats = annotation_status["Assembly Stats"].sum()
    total_bin_mash_sketching = annotation_status["Bin Mash"].sum()
    total_deeparg = annotation_status["DeepARG"].sum()
    total_rrna = annotation_status["RRNA"].sum()
    total_rubisco_hmm = annotation_status["Rubisco_HMM"].sum()
    total_recombinase_hmm = annotation_status["Recombinase_HMM"].sum()
    total_eggnog = annotation_status["Eggnog_Mapper"].sum()
    try:
        cpu_hours = get_cpu_hours()
    except:
        cpu_hours = 0

    summary = {
        "total_studies": str(total_studies),
        "total_samples": str(total_raw_data),
        "total_assemblies": str(total_assemblies),
        "total_coassemblies": str(total_coassemblies),
        "total_psa_psb_bins": str(total_psa_psb_bins),
        "total_samples_with_psa_psb_bins": str(total_samples_with_psa_psb_bins),
        "total_samples_with_no_bins_expected": str(len(sample_bin_blacklist)),
        "total_samples_with_no_assemblies_expected": str(
            len(sample_assembly_blacklist)
        ),
        "total_samples_with_prodigal": str(total_prodigal),
        "total_samples_with_macrel": str(total_macrel),
        "total_samples_with_checkm": str(total_checkm),
        "total_samples_with_checkm2": str(total_checkm2),
        "total_samples_with_gunc": str(total_gunc),
        "total_samples_with_motus": str(total_motus),
        "total_samples_with_taxonomy": str(total_taxonomy),
        "total_abricate": str(total_abricate),
        "total_assembly_mash_sketching": str(total_assembly_mash_sketching),
        "total_assembly_stats": str(total_assembly_stats),
        "total_bin_mash_sketching": str(total_bin_mash_sketching),
        "total_deeparg": str(total_deeparg),
        "total_rrna": str(total_rrna),
        "total_samples_with_rubisco_hmm": str(total_rubisco_hmm),
        "total_samples_with_recombinase_hmm": str(total_recombinase_hmm),
        "total_samples_with_eggnog_mapper": str(total_eggnog),
        "cpu_hours": cpu_hours,
    }
    print(summary)
    add_dict_to_json(summary, "summary.json")
    make_plot()
    make_status_plot(summary)
    make_status_plot2(summary)

    make_donut_plot(
        total_raw_data, total_motus, os.path.join(out_dir, "static/taxonomy_status.svg")
    )
    make_donut_plot(
        total_samples_with_psa_psb_bins,
        total_checkm2,
        os.path.join(out_dir, "static/checkm_status.svg"),
    )
    make_donut_plot(
        total_assemblies,
        total_assembly_mash_sketching,
        os.path.join(out_dir, "static/assembly_mash_status.svg"),
    )
    make_donut_plot(
        total_samples_with_psa_psb_bins,
        total_bin_mash_sketching,
        os.path.join(out_dir, "static/bin_mash_status.svg"),
    )
