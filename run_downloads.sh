#!/bin/bash
#set -e
#cd /g/scb/bork_old/data/ebi_mirror/ena-mirror
#cd /g/scb/bork/data/ebi_mirror/ena-mirror
cd /g/bork5/data/ebi_mirror/ena-mirror
while true
do
  echo '[RUNNING]'
    reqSpace=15000000000 # 15T
    #availSpace=$(df /g/scb/bork_old/data | awk 'NR==2 { print $4 }')
    availSpace=$(df /g/scb/bork/data | awk 'NR==2 { print $4 }')
    if (( availSpace < reqSpace )); then
      echo "not enough Space" >&2
      exit 1
    fi
  /g/scb2/bork/mocat/software/python/3.6-modules-v2/bin/jug execute --verbose debug
  sleep 900
done

