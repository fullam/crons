#!/bin/sh
set -e
WORK_DIR="/g/scb2/bork/fullam/bin/crons/info_portal_data/"


echo "[INFO] Updating CPU hours.."
# CPU Hours
find /g/scb2/bork/fullam/mags-logs/ -name "slurm*" -exec grep -F "CPU hours" {} \; > ${WORK_DIR}/cpu_hours_raw.txt
/g/bork3/home/fullam/miniconda3/envs/we/bin/python /g/scb2/bork/fullam/bin/crons/get_cpu_hours_mags_pipeline.py > ${WORK_DIR}/cpu_hours_total.txt

# echo "[INFO] Updating gene counts.."
# # Gene Counts
# find /g/scb2/bork/data/MAGs/annotations/*/psa_megahit/prodigal/ -name "*.prodigal.faa.gz" -print0 | xargs  -0 -P 20 -n 1 grep -FHc ">" | awk -F':' '{sum+=$2;} END{print sum;}' > ${WORK_DIR}/gene_counts_per_sample.txt

# awk -F':' '{sum+=$2;} END{print sum;}' ${WORK_DIR}/gene_counts_per_sample.txt > ${WORK_DIR}/gene_counts_total.txt


# echo "[INFO] Updating motus data.."
# # Update motus merge data
# /g/bork3/home/fullam/miniconda3/envs/we/bin/python /g/scb2/bork/fullam/development/motus_merge/update_motus_merge.py
# rsync -r /g/scb2/bork/fullam/development/motus_merge/merged_motus_mg3_web.tsv fullam@metalog:/srv/info_portal/motus_data/all_motus2.5.mg3.relabund.tsv



