#!/usr/bin/env python3

hours = []
with open('/g/scb2/bork/fullam/bin/crons/info_portal_data/cpu_hours_raw.txt', 'r') as f:
    for line in f:
        hours.append(line.replace('CPU hours   :', '').split(' (')[0].replace("'", "").replace(' ', ''))

print(round(sum([float(x) for x in hours if x]), 2))
