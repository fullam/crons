import os
import sys
import glob
import socket
import pymongo
import traceback
import pandas as pd
from datetime import datetime


def read_fq_stats_file(fq_stats_file):
    df = pd.read_csv(fq_stats_file, sep='\t')
    pre_filtering = df[~df['file'].str.startswith("preproc")]
    post_filtering = df[df['file'].str.startswith("preproc")]
    return pre_filtering, post_filtering


def weighted_mean(values, weights):
    weighted_sum = []
    for value, weight in zip(values, weights):
        weighted_sum.append(value * weight)
    return round(sum(weighted_sum) / sum(weights), 3)


def parse_fq_stats(data):
    if len(data) == 0:
        return {
            'sequence_count': None,
            'base_count': None,
            'min_seq_len': None,
            'max_seq_len': None,
            'gc_percent': None,
            'insert_count': None,
        }
    metadata = {}
    metadata['sequence_count'] = int(data['numSeqs'].sum())
    metadata['base_count'] = int(data['numBasepairs'].sum())
    metadata['min_seq_len'] = float(round(data['minSeqLen'].mean()))
    metadata['max_seq_len'] = float(round(data['maxSeqLen'].mean()))
    metadata['gc_percent'] = weighted_mean(data['gcContent'], data['numBasepairs'])
    insert_df = data[~data['file'].str.contains("pair.2|pairs.2")]
    metadata['insert_count'] = int(insert_df['numSeqs'].sum())
    return metadata


def parse_motus_file_header(motus_file):
    run_data = {}
    with open(motus_file, 'r') as f:
        for line in f:
            line = line.strip()
            if '# Script run on' in line:
                str_date = line.replace('# Script run on ', '')
                run_data['run_date'] = datetime.strptime(str_date, '%a %d-%m-%Y %H:%M')
            if '# Output hash:' in line:
                run_data['ngless_hash'] = line.replace('# Output hash: ', '')
                return run_data
            if not line.startswith('#'):
                return run_data


def read_motus_file(motus_file):
    return_data = {}
    with open(motus_file, 'r') as f:
        for i, line in enumerate(f):
            if not line.startswith('#'):
                break
    df = pd.read_csv(motus_file, skiprows=i, sep='\t', index_col=0)
    # df.index = df.index.str.replace(' ', '_').str.replace("'", '').str.replace('.', '_')
    df.index.values[df.index.get_loc('-1')] = ' [-1]'
    df.index = df.index.str.extract(' \[(.+?)?\]$', expand=False)
    for sample_id in df.columns:
        return_data[sample_id] = df[sample_id][df[sample_id] != 0].to_dict()
    return return_data


def parse_motus_output(motus_file):
    return_data = {}
    for motus_file in motus_files:
        return_data[motus_file] = {}
    for motus_file in motus_files:
        return_data[motus_file]['header'] = parse_motus_file_header(
            motus_files[motus_file][0]
        )
        return_data[motus_file]['data'] = read_motus_file(motus_files[motus_file][0])
    return return_data


def add_field_to_sample(db, sample_id, data):
    db.samples.update_one({'sample_id': sample_id}, {'$set': data})


if __name__ == "__main__":
    motus_version = '2.5'

    if socket.gethostname() != 'eta.embl.de':
        sys.exit('[ERROR] must be run on eta >:(')

    client = pymongo.MongoClient(
        'mongodb://mag_readwrite:password_mag_readwrite@127.0.0.1:26016'
    )
    db = client['mags']

    motus_dirs = [sys.argv[1]]

    for motus_dir in motus_dirs:
        study = motus_dir.split("/")[-1]
        if any(
            item in study for item in ['test', 'metaT', 'metaT', '16S', '.mocatFormat']
        ):
            sys.exit(f'[WARNING] {study} skipped!!')
        print(f'[STUDY] {study} should be added')
        try:
            fq_stats_files = glob.glob(
                os.path.join(motus_dir, 'ngless-stats/*/*/fq.tsv')
            )
            motus_files = {
                'motus_mg2_relabund': glob.glob(
                    os.path.join(motus_dir, 'outputs/*.mg2.relabund.tsv')
                ),
                'motus_mg3_relabund': glob.glob(
                    os.path.join(motus_dir, 'outputs/*.mg3.relabund.tsv')
                ),
                'motus_mg2_insert_count': glob.glob(
                    os.path.join(motus_dir, 'outputs/*.mg2.insertcount.tsv')
                ),
                'motus_mg3_insert_count': glob.glob(
                    os.path.join(motus_dir, 'outputs/*.mg3.insertcount.tsv')
                ),
            }
            motus_data = parse_motus_output(motus_files)

            parsed_data = {}
            for fq_stats_file in fq_stats_files:
                sample_id = fq_stats_file.split('/')[-2]
                pre_filtering, post_filtering = read_fq_stats_file(fq_stats_file)
                pre_filtering_data = parse_fq_stats(pre_filtering)
                post_filtering_data = parse_fq_stats(post_filtering)
                try:
                    parsed_data[sample_id] = {
                        'pre_filtering': pre_filtering_data,
                        'post_filtering': post_filtering_data,
                        'motus_version': motus_version,
                        'motus_mg2_relabund': {
                            **motus_data['motus_mg2_relabund']['header'],
                            'data': motus_data['motus_mg2_relabund']['data'][sample_id],
                        },
                        'motus_mg3_relabund': {
                            **motus_data['motus_mg3_relabund']['header'],
                            'data': motus_data['motus_mg3_relabund']['data'][sample_id],
                        },
                        'motus_mg2_insert_count': {
                            **motus_data['motus_mg2_insert_count']['header'],
                            'data': motus_data['motus_mg2_insert_count']['data'][
                                sample_id
                            ],
                        },
                        'motus_mg3_insert_count': {
                            **motus_data['motus_mg3_insert_count']['header'],
                            'data': motus_data['motus_mg3_insert_count']['data'][
                                sample_id
                            ],
                        },
                    }
                except KeyError:
                    print(f'{sample_id} missing in motus profile')
            added_to_db = []
            not_added_to_db = []
            for sample_id in parsed_data:
                if db.samples.find_one({'sample_id': sample_id}):
                    # db.samples.update_one(
                    #     {'sample_id': sample_id},
                    #     {'$set': {'motus_25': parsed_data[sample_id]}},
                    # )
                    added_to_db.append(sample_id)
                else:
                    not_added_to_db.append((study, sample_id))
                    print(f'[MISSING IN DB] {study} {sample_id}')
        except Exception as e:
            print(e, traceback.print_tb(e.__traceback__))

print(f'MOTUS SAMPLES ADDED TO DB: {len(added_to_db)}')
print(f'MOTUS SAMPLES NOT ADDED TO DB: {len(not_added_to_db)}')
