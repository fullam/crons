#!/bin/sh -x

cd /g/scb/bork/data/
chgrp -R bork-data /g/scb/bork/data/spire

find /g/scb/bork/data/spire/ -type d -print0 | xargs -0 chmod g+rwxs
find /g/scb/bork/data/spire/ -type f -print0 | xargs -0 chmod g+rw

find /g/scb/bork/data/spire/ -type d -print0 | xargs -0 nfs4_setfacl -A /g/scb/bork/data/spire/permissions/dir_acl.txt
find /g/scb/bork/data/spire/ -type f -print0 | xargs -0 nfs4_setfacl -A /g/scb/bork/data/spire/permissions/file_acl.txt
