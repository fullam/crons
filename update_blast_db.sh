echo "Running blast db update.."
if [ -f /etc/profile.d/modules.sh ]; then
     source /etc/profile.d/modules.sh
fi


if declare -f module > /dev/null ; then
        module load
        module use --append /g/scb2/bork/mocat/software/bork_modulefiles/
        #export MODULEPATH=$MODULEPATH:/g/scb2/bork/mocat/software/bork_modulefiles/
fi

module load Perl
cd /g/scb2/bork/mocat/databases/blast/swissprot
/g/scb2/bork/mocat/databases/blast/ncbi-blast-2.15.0+/bin/update_blastdb.pl --decompress swissprot --num_threads 4 --source ncbi
cd /g/scb2/bork/mocat/databases/blast/nt
/g/scb2/bork/mocat/databases/blast/ncbi-blast-2.15.0+/bin/update_blastdb.pl --decompress nt --num_threads 4 --source ncbi
cd /g/scb2/bork/mocat/databases/blast/nr
/g/scb2/bork/mocat/databases/blast/ncbi-blast-2.15.0+/bin/update_blastdb.pl --decompress nr --num_threads 4 --source ncbi

rsync -av /g/scb2/bork/mocat/databases/blast/swissprot /g/scb2/bork/mocat/databases/blast/archive/swissprot_`date +\\%Y-\\%m-\\%d`
rsync -av /g/scb2/bork/mocat/databases/blast/nt /g/scb2/bork/mocat/databases/blast/archive/nt_`date +\\%Y-\\%m-\\%d`
rsync -av /g/scb2/bork/mocat/databases/blast/nr /g/scb2/bork/mocat/databases/blast/archive/nr_`date +\\%Y-\\%m-\\%d`

find /g/scb2/bork/mocat/databases/blast/archive/ -type d -maxdepth 1 -mindepth 1 -type d -mtime +100 -exec echo Delete: {} \;

