#!/usr/bin/env python3
import sys
import time
import smtplib
import requests
import pandas as pd
from io import StringIO
from datetime import datetime
from email.message import EmailMessage


def send_mail(message, recipients):
    if not recipients:
        return
    print(message)
    for recipient in ["anthony.fullam@embl.de", recipients]:
        msg = EmailMessage()
        msg.set_content(message)
        msg["Subject"] = "[ENA Study Watcher]"
        msg["From"] = "ena_watcher@embl.de"
        msg["To"] = recipient
        s = smtplib.SMTP("smtp.embl.de", 25)
        s.send_message(msg)
        s.quit()


def load_data(watchlist_filepath):
    df = pd.read_csv(watchlist_filepath, sep="\t", comment="#", header=0, index_col=0)
    df = df.dropna(how="all")
    return df.to_dict(orient="index")


def make_request(url, params):
    for i in range(4):
        try:
            response = requests.get(url, params=params, timeout=60)
            break
        except Exception as e:
            print("Try {0} failed: {1} {2} {3}".format(i, url, params, e))
            time.sleep(i * 1)
    else:
        return ""
    if response.ok:
        return response.text
    else:
        sys.exit("ENA query Failed.")


def check_ena_result_count(url, params):
    params = dict(params)
    params["resultcount"] = ""
    params["display"] = ""
    count_query_result = make_request(url, params)
    return int(count_query_result.split()[3].replace(",", ""))


def ena_query(url, params, count=False):
    results = []
    if count:
        result_count = check_ena_result_count(url, params)
        offsets = int(result_count / 100000) + int(result_count % 100000 > 0)
        for offset in range(offsets):
            params["offset"] = offset * 100001
            params["length"] = 100000
            results.append(make_request(url, params))
        return results
    else:
        return [make_request(url, params)]


def get_runreport_for_study(study_accession, ena_filereport_url):
    """Get ENA run report for a study accession.

    Args:
        study_accession (str): ENA study accession number

    Returns:
        str: Run report

    """
    params = {"accession": study_accession, "result": "read_run"}
    run_report = ena_query(ena_filereport_url, params, count=False)[0]
    if run_report:
        return pd.read_csv(StringIO(run_report), delimiter="\t")
    else:
        return pd.DataFrame()


def check_if_data_in_ena(study_accession):
    run_report = get_runreport_for_study(study_accession, ena_filereport_url)
    if len(run_report) == 0:
        return False
    elif len(run_report["fastq_ftp"].dropna().tolist()) == 0:
        return False
    return True


def notification_msg(run_report, study_accession):
    if len(run_report) == 0:
        return f"{study_accession} - Study still not on ENA"
    if len(run_report["fastq_ftp"].dropna().tolist()) == 0:
        return f"{study_accession} - Study on ENA but no fastq data"
    return f"{study_accession} - Data appeared on ENA!!"


def should_notify(date_str):
    notify_date = datetime.strptime(date_str, "%Y-%m-%d")
    should_notify = notify_date <= datetime.today()
    return should_notify


def remove_studies(study_watchlist, studies_to_remove):
    print(f'[REMOVING] {studies_to_remove}')
    df = pd.DataFrame.from_dict(study_watchlist, orient="index")
    return df.drop(studies_to_remove)


def write_studies_to_check_file(study_watchlist, watchlist_filepath):
    tsv = study_watchlist.to_csv(header=False, sep="\t")
    with open(watchlist_filepath, "w") as f:
        f.write("# Add studies to this table to be notified if they appear on ENA\n")
        f.write("#\n")
        f.write(
            "# If study appears on ENA or expiry date is reached - email address will be notified\n"
        )
        f.write("#\n")
        f.write("# Data should be entered tab-separated as follows:\n")
        f.write("#     ENA_ACCESSION - Study to watch, e.g. PRJEB38078\n")
        f.write(
            "#     EXPIRY_DATE - Date when to stop watching for study - in format 2020-07-03 (with leading zeroes)\n"
        )
        f.write(
            "#     EMAIL - separated with comma if multiple emails given (no spaces)\n"
        )
        f.write("ENA_ACCESSION\tEXPIRY_DATE\tEMAIL\n")
        f.write(tsv)


if __name__ == "__main__":
    ena_search_url = "http://www.ebi.ac.uk/ena/portal/api/search"
    ena_filereport_url = "http://www.ebi.ac.uk/ena/portal/api/filereport"
    watchlist_filepath = "/g/bork5/mocat/ena_watchlist.tsv"

    study_watchlist = load_data(watchlist_filepath)
    print(study_watchlist)
    studies_to_remove = []

    for study_accession in study_watchlist:
        run_report = get_runreport_for_study(study_accession, ena_filereport_url)
        recipient = study_watchlist[study_accession]["EMAIL"]
        if check_if_data_in_ena(study_accession):
            send_mail(f"{study_accession} appeared on ENA!!", recipient)
            studies_to_remove.append(study_accession)
        else:
            if should_notify(study_watchlist[study_accession]["EXPIRY_DATE"]):
                send_mail(f"{study_accession} - Study still not on ENA", recipient)
                studies_to_remove.append(study_accession)

    new_watchlist = remove_studies(study_watchlist, studies_to_remove)
    write_studies_to_check_file(new_watchlist, watchlist_filepath)
