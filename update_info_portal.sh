#!/bin/sh
# set -e
# echo "[INFO] Updating data.."
# /g/scb2/bork/fullam/bin/Miniconda3/envs/we/bin/python /g/scb2/bork/fullam/bin/info_portal/scripts/update_status_pages.py
# echo "[INFO] Syncing to metalog.."
# rsync -r /g/scb2/bork/fullam/bin/info_portal/tables/* fullam@metalog:/srv/info_portal/tables/
# rsync -r /g/scb2/bork/fullam/bin/info_portal/motus_data/* fullam@metalog:/srv/info_portal/motus_data/
# rsync -r /g/scb2/bork/fullam/bin/info_portal/static/summary.png fullam@metalog:/srv/info_portal/static/

#echo "[INFO] Sync logfiles.."
#rsync -av --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/assemblies  /g/scb2/bork/fullam/mags-logs/
#rsync -av --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/co_assemblies  /g/scb2/bork/fullam/mags-logs/
#rsync -av --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/ps_binning  /g/scb2/bork/fullam/mags-logs/
#rsync -av --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/annotation  /g/scb2/bork/fullam/mags-logs/
#rsync -av --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/coa_binning  /g/scb2/bork/fullam/mags-logs/
#rsync -av --exclude 'work_dir' --exclude '.nextflow' /scratch/fullam/ps_assembly_binning  /g/scb2/bork/fullam/mags-logs/
#find /scratch/fullam/references  -exec touch {} +
#find /scratch/fullam/mags-pipeline/  -exec touch {} +
#echo "[INFO] Done!"
